// 	var theForm = document.getElementById( 'theForm' );

// 		new stepsForm( theForm, {
// 			onSubmit : function( form ) {
// 				// hide form
// 				classie.addClass( theForm.querySelector( '.simform-inner' ), 'hide' );

// 				/*
// 				form.submit()
// 				or
// 				AJAX request (maybe show loading indicator while we don't have an answer..)
// 				*/

// });
$("#theForm").on("submit", function(e) {
            e.preventDefault();
            var regexp = /[^a-zA-Z]/g;
            if ($('.questions li.current').index() == 0) {
                if (!$('#q1').val().match(regexp) && $('#q1').val().length !== 0) {
                    $('#q1').parent().removeClass('current').next().addClass('current');
                    $('.error-message').removeClass('show').html(' ');
                    $('.simform .progress').css('width', '33.333%');
                } else {
                    $('.error-message').addClass('show').html('Please enter your valid Name!');
                    $('#theForm').addClass('invalid');
                    setTimeout(function() {
                    	$('#theForm').removeClass('invalid');
                     }, 400);
                }
            } else if ($('.questions li.current').index() == 1) {
                if (IsEmail($('#q2').val()) == true && $('#q2').val().length !== 0) {
                    $('#q2').parent().removeClass('current').next().addClass('current');
                    $('.error-message').removeClass('show').html(' ');
                    $('.simform .progress').css('width', '66.666%');

                } else {
                    $('.error-message').addClass('show').html('Please enter your valid Email!');
                    $('#theForm').addClass('invalid');
                    
                    setTimeout(function() {
                    	$('#theForm').removeClass('invalid');
                     }, 400);
                }
            } else if ($('.questions li.current').index() == 2) {
                if ($('#q3').val().length !== 0) {
                    $('#q3').parent().removeClass('current').next().addClass('current');
                    $('.error-message').removeClass('show').html(' ');
                    setTimeout(function() {
                    	$('.simform-inner').hide();
                    }, 400);
                    $('.simform .progress').css('width', '100%');
                    var q1 = $('#q1').val();
                    var q2 = $('#q2').val();
                    var q3 = $('#q3').val();
                    setTimeout(function() {
                            $.ajax({
                                url: '../jd-solutions/php/contact-form.php',
                                type: 'post',
                                data: { q1: q1, q2: q2, q3: q3 },
                                success: function(response) {
                                    console.log(response);
                                },
                                error: function(xhr, desc, err) {
                                    console.log('error');
                                }
                            }); // end ajax call
                            // let's just simulate something...
                            var messageEl = $('.final-message');
                            messageEl.html('Thank you, ' + q1 + '!');
                            messageEl.addClass('show');
                        }, 400);

                    }
                    else {
                        
                    }
                }
            });

        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(email)) {
                return false;
            } else {
                return true;
            }
        }
