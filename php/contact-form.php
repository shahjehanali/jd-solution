<?php

$q1 = $_POST['q1'];
$q2 = $_POST['q2'];
$q3 = $_POST['q3'];

  $to = 'peter@jouret-development.be';
	$subject = 'Email From JDSolutions';

	$name = filter_var( $q1, FILTER_SANITIZE_STRING );


	$usermail = filter_var( $q2, FILTER_SANITIZE_EMAIL );


	$phone = filter_var( $q3, FILTER_SANITIZE_NUMBER_INT );

	include_once( 'class.phpmailer.php' );
	$mail = new PHPMailer;
	$mail->isMail();
	$mail->AddAddress('peter@jouret-development.be');

	$mail->Subject = "Mail Contact Form Submission.";

	$message     = '<html><body>';
	$message    .= '<em>This email is comes from JDSolutions.<br><br><br>';
	$message    .= '<table style="border:0; vertical-align:top;"><tr><td  valign="top"><strong>Name : </strong></td><td>'.$name.'</td></tr>';
	$message    .= '<tr><td  valign="top"><strong>Email : </strong></td><td>'.$usermail.'</td></tr>';
	$message    .= '<tr><td  valign="top"><strong>Message : </strong></td><td>'.$phone.'</td></tr></table><br>';
	$message    .= '<br><br>Best Regards,<br><br>Team <br><br></em>';
	$message    .= '</body></html>';
	$mail->msgHTML($message);
	if( $mail->Send() ){

		$mail->clearAddresses();
		echo 'Email has beed sent!';
		return true;
	}else{
		echo 'Email not sent!';
		$mail->clearAddresses();
		echo 'Mailer Error: ' . $mail->ErrorInfo;
		return false;
	}

?>
